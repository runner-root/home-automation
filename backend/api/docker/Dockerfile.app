ARG CONTAINER_REGISTRY='Variable not set'
ARG PROJECT_SLUG='Variable not set'
ARG STAGE='Variable not set'

FROM ${CONTAINER_REGISTRY}${PROJECT_SLUG}-api-dep:${STAGE}

# copy app
COPY ./app /app
