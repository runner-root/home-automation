#! /usr/bin/env sh

# Let the DB start
python /app/app/check_dependencies.py

# Create initial data in DB
python /app/app/initialize_data.py
