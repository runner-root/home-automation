from app import schemas
from app.core.settings import settings
from app.databases.timescaledb import base, crud
from app.databases.timescaledb.session import engine
from sqlalchemy.orm import Session

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


def init_db(timescaledb: Session) -> None:
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    base.Base.metadata.create_all(bind=engine)

    # handle initial superuser
    superuser = crud.user.get_by_email(timescaledb, email=settings.SUPER_USER_EMAIL)
    if not superuser:
        user_in = schemas.UserCreate(
            email=settings.SUPER_USER_EMAIL,
            username=settings.SUPER_USER_USERNAME,
            password=settings.SUPER_USER_PASSWORD,
        )
        superuser = crud.user.create(timescaledb, schema_in=user_in)

    # handle superuser group
    superuser_group = crud.group.get_by_name(timescaledb, group_name="superusers")
    if not superuser_group:
        group_in = schemas.GroupCreate(name="superusers")
        superuser_group = crud.group.create(timescaledb, schema_in=group_in)

    # add user to superusers
    if superuser not in superuser_group.users:
        updated_group = crud.group.add_user(
            timescaledb, group_in=superuser_group, user_in=superuser
        )
