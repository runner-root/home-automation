from typing import List

from app import models, schemas
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session


class CRUDPlant:
    def get_all(self, timescaledb: Session) -> List[models.Plant]:
        return timescaledb.query(models.Plant).all()

    def get_by_name(self, timescaledb: Session, plant_name: str) -> models.Plant:
        return (
            timescaledb.query(models.Plant)
            .filter(models.Plant.name == plant_name)
            .first()
        )

    def get_by_id(self, timescaledb: Session, plant_id: int) -> models.Plant:
        return (
            timescaledb.query(models.Plant).filter(models.Plant.id == plant_id).first()
        )

    def create(
        self, timescaledb: Session, schema_in: schemas.PlantCreate
    ) -> models.Plant:
        plant_in_data = jsonable_encoder(schema_in)
        created_plant = models.Plant(**plant_in_data)
        timescaledb.add(created_plant)
        timescaledb.commit()
        timescaledb.refresh(created_plant)
        return created_plant

    def update(
        self,
        timescaledb: Session,
        model_in: models.Plant,
        schema_in: schemas.PlantUpdate,
    ) -> models.Plant:

        model_updated = model_in

        if schema_in.name is not None:
            model_updated.name = schema_in.name
        if schema_in.species is not None:
            model_updated.species = schema_in.species
        if schema_in.location is not None:
            model_updated.location = schema_in.location

        timescaledb.commit()
        timescaledb.refresh(model_updated)

        return model_updated


plant = CRUDPlant()
