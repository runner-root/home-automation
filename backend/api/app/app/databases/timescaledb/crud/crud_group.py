from typing import List, Optional

from app import models, schemas
from sqlalchemy.orm import Session


class CRUDGroup:
    def get_all(self, timescaledb: Session) -> List[models.Group]:
        return timescaledb.query(models.Group).all()

    def get_by_id(self, timescaledb: Session, group_id: int) -> Optional[models.Group]:
        return (
            timescaledb.query(models.Group).filter(models.Group.id == group_id).first()
        )

    def get_by_name(
        self, timescaledb: Session, group_name: str
    ) -> Optional[models.Group]:
        return (
            timescaledb.query(models.Group)
            .filter(models.Group.name == group_name)
            .first()
        )

    def create(
        self, timescaledb: Session, schema_in: schemas.GroupCreate
    ) -> models.Group:
        timescaledb_obj = models.Group(name=schema_in.name)
        timescaledb.add(timescaledb_obj)
        timescaledb.commit()
        timescaledb.refresh(timescaledb_obj)
        return timescaledb_obj

    def update(
        self,
        timescaledb: Session,
        model_in: models.Group,
        schema_in: schemas.GroupUpdate,
    ) -> models.Group:
        model_updated = model_in

        # can be set by self
        if schema_in.name is not None:
            model_updated.name = schema_in.name

        timescaledb.commit()
        timescaledb.refresh(model_updated)

        return model_updated

    def add_user(
        self,
        timescaledb: Session,
        group_in: models.Group,
        user_in: models.User,
    ) -> models.Group:
        # can be set by self
        group_in.users.append(user_in)

        timescaledb.commit()
        timescaledb.refresh(group_in)

        return group_in

    def remove_user(
        self,
        timescaledb: Session,
        group_in: models.Group,
        user_in: models.User,
    ) -> models.Group:
        group_in.users.remove(user_in)
        timescaledb.commit()
        timescaledb.refresh(group_in)

        return group_in

    def add_plant(
        self,
        timescaledb: Session,
        group_in: models.Group,
        plant_in: models.Plant,
    ) -> models.Group:
        # can be set by self
        group_in.plants.append(plant_in)

        timescaledb.commit()
        timescaledb.refresh(group_in)

        return group_in

    def remove_plant(
        self,
        timescaledb: Session,
        group_in: models.Group,
        plant_in: models.User,
    ) -> models.Group:
        group_in.plants.remove(plant_in)
        timescaledb.commit()
        timescaledb.refresh(group_in)

        return group_in


group = CRUDGroup()
