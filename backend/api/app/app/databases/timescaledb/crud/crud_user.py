from typing import List, Optional, Union

from app import models, schemas
from app.core import security
from sqlalchemy.orm import Session


class CRUDUser:
    def get_all(self, timescaledb: Session) -> List[models.User]:
        return timescaledb.query(models.User).all()

    def get_by_id(self, timescaledb: Session, user_id: int) -> Optional[models.User]:
        return timescaledb.query(models.User).filter(models.User.id == user_id).first()

    def get_by_email(self, timescaledb: Session, email: str) -> Optional[models.User]:
        return timescaledb.query(models.User).filter(models.User.email == email).first()

    def get_by_username(
        self, timescaledb: Session, username: str
    ) -> Optional[models.User]:
        return (
            timescaledb.query(models.User)
            .filter(models.User.username == username)
            .first()
        )

    def create(
        self, timescaledb: Session, schema_in: schemas.UserCreate
    ) -> models.User:
        timescaledb_obj = models.User(
            email=schema_in.email,
            username=schema_in.username,
            hashed_password=security.get_password_hash(schema_in.password),
        )
        timescaledb.add(timescaledb_obj)
        timescaledb.commit()
        timescaledb.refresh(timescaledb_obj)
        return timescaledb_obj

    def update(
        self,
        timescaledb: Session,
        model_in: models.User,
        schema_in: Union[schemas.UserUpdate, schemas.UserSuperuserUpdate],
    ) -> models.User:
        model_updated = model_in

        # can be set by self
        if schema_in.username is not None:
            model_updated.username = schema_in.username
        if schema_in.email is not None:
            model_updated.email = schema_in.email
        if schema_in.password is not None:
            model_updated.hashed_password = security.get_password_hash(
                schema_in.password
            )
        # can only be set by superuser
        if isinstance(schema_in, schemas.UserSuperuserUpdate):
            if schema_in.is_active is not None:
                model_updated.is_active = schema_in.is_active

        timescaledb.commit()
        timescaledb.refresh(model_updated)

        return model_updated

    def authenticate(
        self, timescaledb: Session, username: str, password: str
    ) -> Optional[models.User]:
        user = self.get_by_username(timescaledb, username=username)
        if not user:
            return None
        if not security.verify_password(password, user.hashed_password):
            return None
        return user

    def is_active(self, user_model: models.User) -> bool:
        return user_model.is_active

    def is_superuser(self, user_model: models.User) -> bool:
        for group in user_model.groups:
            if group.id == 1:
                return True

        return False

    def has_permission_for_plant(
        self, user_model: models.User, plant_model: models.Plant
    ) -> bool:
        if self.is_superuser(user_model):
            return True

        for group in plant_model.groups:
            if group in user_model.groups:
                return True

        return False


user = CRUDUser()
