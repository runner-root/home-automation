from datetime import datetime, timedelta
from typing import List

from app import models, schemas
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session


class CRUDMeasurement:
    def create_with_plant(
        self, timescaledb: Session, schema_in: schemas.MeasurementCreate, plant_id: int
    ) -> models.Measurement:
        measurement_in_data = jsonable_encoder(schema_in)
        created_measurement = models.Measurement(
            **measurement_in_data, plant_id=plant_id, created_utc=datetime.utcnow()
        )
        timescaledb.add(created_measurement)
        timescaledb.commit()
        timescaledb.refresh(created_measurement)
        return created_measurement

    def get_since_by_plant(
        self, timescaledb: Session, plant_id: int, past_x_minutes: int
    ) -> List[models.Measurement]:
        since_measured = datetime.utcnow() - timedelta(minutes=past_x_minutes)
        measurements = (
            timescaledb.query(models.Measurement)
            .filter(models.Measurement.plant_id == plant_id)
            .filter(models.Measurement.created_utc >= since_measured)
            .all()
        )

        return measurements


measurement = CRUDMeasurement()
