from typing import Generator

from app import models, schemas
from app.core import exceptions, security
from app.core.settings import settings
from app.databases.timescaledb import crud
from app.databases.timescaledb.session import SessionLocal
from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from jose.exceptions import ExpiredSignatureError
from pydantic import ValidationError
from sqlalchemy.orm import Session

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl=f"{settings.API_V1_ROOT_PATH}/account_management/users/get_token"
)


def get_timescaledb() -> Generator:
    try:
        timescaledb = SessionLocal()
        yield timescaledb
    finally:
        timescaledb.close()


def get_current_user(
    timescaledb: Session = Depends(get_timescaledb), token: str = Depends(oauth2_scheme)
) -> models.User:
    try:
        payload: dict = jwt.decode(
            token, settings.API_SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        token_data: schemas.TokenPayload = schemas.TokenPayload(**payload)
    except (jwt.JWTError, ValidationError):
        raise exceptions.invalid_credentials
    except ExpiredSignatureError:
        raise exceptions.session_expired
    user = crud.user.get_by_id(timescaledb, user_id=token_data.sub)
    if user is None:
        raise exceptions.invalid_credentials
    return user


def get_current_active_user(
    current_user: models.User = Depends(get_current_user),
) -> models.User:
    if not crud.user.is_active(current_user):
        raise exceptions.inactive_user
    return current_user


def get_current_active_superuser(
    current_active_user: models.User = Depends(get_current_active_user),
) -> models.User:
    if not crud.user.is_superuser(current_active_user):
        raise exceptions.insufficient_permissions
    return current_active_user
