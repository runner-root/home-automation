from datetime import timedelta
from typing import Dict

from app import models, schemas
from app.api.deppendencies import (
    get_current_active_superuser,
    get_current_active_user,
    get_timescaledb,
)
from app.core import exceptions, security
from app.core.settings import settings
from app.databases.timescaledb import crud
from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

router = APIRouter(prefix="/users")


@router.get("/me", response_model=schemas.User)
async def get_user_me(
    current_active_user: models.User = Depends(get_current_active_user),
) -> schemas.User:
    return schemas.User.from_orm(current_active_user)


@router.put("/me", response_model=schemas.User)
def update_user_me(
    user_in: schemas.UserUpdate,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_user: models.User = Depends(get_current_active_user),
) -> schemas.User:
    # check if username or email exists
    if crud.user.get_by_username(timescaledb, username=user_in.username):
        raise exceptions.username_exists
    if crud.user.get_by_email(timescaledb, email=user_in.email):
        raise exceptions.email_exists

    updated_user = crud.user.update(
        timescaledb, model_in=current_active_user, schema_in=user_in
    )

    return schemas.User.from_orm(updated_user)


@router.post("/get_token", response_model=schemas.Token)
async def get_access_token_by_login(
    timescaledb: Session = Depends(get_timescaledb),
    form_data: OAuth2PasswordRequestForm = Depends(),
) -> dict:
    user = crud.user.authenticate(
        timescaledb, username=form_data.username, password=form_data.password
    )
    if not user:
        raise exceptions.invalid_credentials
    access_token_expires: timedelta = timedelta(
        minutes=settings.API_ACCESS_TOKEN_EXPIRE_MINUTES
    )
    access_token: str = security.create_access_token(
        subject=user.id, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@router.post("/create", response_model=schemas.User)
async def create_user(
    user_to_create: schemas.UserCreate,
    current_active_superuser: models.User = Depends(get_current_active_superuser),
    timescaledb: Session = Depends(get_timescaledb),
) -> schemas.User:

    created_user = crud.user.create(timescaledb, schema_in=user_to_create)

    if not created_user:
        raise exceptions.database_writing

    return schemas.User.from_orm(created_user)


@router.get("/", response_model=Dict[int, str])
async def get_all_user_names(
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> Dict[int, str]:
    users = crud.user.get_all(timescaledb)
    return {
        schemas.User.from_orm(user).id: schemas.User.from_orm(user).username
        for user in users
    }


@router.get("/{user_id}", response_model=schemas.User)
def get_user_by_id(
    user_id: int,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.User:
    user = crud.user.get_by_id(timescaledb, user_id=user_id)
    if not user:
        raise exceptions.not_found
    return schemas.User.from_orm(user)


@router.put("/{user_id}", response_model=schemas.User)
def update_user_by_id(
    user_id: int,
    user_in: schemas.UserSuperuserUpdate,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.User:
    # check if username or email exists
    if crud.user.get_by_username(timescaledb, username=user_in.username):
        raise exceptions.username_exists
    if crud.user.get_by_email(timescaledb, email=user_in.email):
        raise exceptions.email_exists

    user_to_update = crud.user.get_by_id(timescaledb, user_id=user_id)
    if not user_to_update:
        raise exceptions.not_found

    updated_user = crud.user.update(
        timescaledb, model_in=user_to_update, schema_in=user_in
    )

    return schemas.User.from_orm(updated_user)
