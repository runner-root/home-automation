from typing import Dict

from app import models, schemas
from app.api.deppendencies import get_current_active_superuser, get_timescaledb
from app.core import exceptions
from app.databases.timescaledb import crud
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

router = APIRouter(prefix="/groups")


@router.post("/create", response_model=schemas.Group)
async def create_group(
    group_to_create: schemas.GroupCreate,
    current_active_superuser: models.User = Depends(get_current_active_superuser),
    timescaledb: Session = Depends(get_timescaledb),
) -> schemas.Group:
    # check if group name exists
    if crud.group.get_by_name(timescaledb, group_name=group_to_create.name):
        raise exceptions.group_name_exists

    created_group = crud.group.create(timescaledb, schema_in=group_to_create)

    if not created_group:
        raise exceptions.database_writing

    return schemas.Group.from_orm(created_group)


@router.get("/", response_model=Dict[int, str])
async def get_all_group_names(
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> Dict[int, str]:
    groups = crud.group.get_all(timescaledb)
    return {
        schemas.Group.from_orm(group).id: schemas.Group.from_orm(group).name
        for group in groups
    }


@router.get("/{group_id}", response_model=schemas.Group)
def get_group_by_id(
    group_id: int,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.Group:
    group = crud.group.get_by_id(timescaledb, group_id=group_id)
    if not group:
        raise exceptions.not_found
    return schemas.Group.from_orm(group)


@router.put("/{group_id}", response_model=schemas.Group)
def update_group_by_id(
    group_id: int,
    group_in: schemas.GroupUpdate,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.Group:
    # check if group name exists
    if crud.group.get_by_name(timescaledb, group_name=group_in.name):
        raise exceptions.group_name_exists

    group_to_update = crud.group.get_by_id(timescaledb, group_id=group_id)
    if not group_to_update:
        raise exceptions.not_found

    updated_group = crud.group.update(
        timescaledb, model_in=group_to_update, schema_in=group_in
    )

    return schemas.Group.from_orm(updated_group)


@router.put("/{group_id}/add_user", response_model=schemas.Group)
def group_add_user(
    group_id: int,
    user_id: int,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.Group:

    group_to_update = crud.group.get_by_id(timescaledb, group_id=group_id)
    user_to_add = crud.user.get_by_id(timescaledb, user_id=user_id)

    if not group_to_update or not user_to_add:
        raise exceptions.not_found

    # check if user already is in group
    if user_to_add in group_to_update.users:
        return schemas.Group.from_orm(group_to_update)

    updated_group = crud.group.add_user(
        timescaledb, group_in=group_to_update, user_in=user_to_add
    )

    return schemas.Group.from_orm(updated_group)


@router.put("/{group_id}/remove_user", response_model=schemas.Group)
def group_remove_user(
    group_id: int,
    user_id: int,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.Group:

    group_to_update = crud.group.get_by_id(timescaledb, group_id=group_id)
    user_to_remove = crud.user.get_by_id(timescaledb, user_id=user_id)

    if not group_to_update or not user_to_remove:
        raise exceptions.not_found

    # check if user is in group
    if user_to_remove not in group_to_update.users:
        return schemas.Group.from_orm(group_to_update)

    updated_group = crud.group.remove_user(
        timescaledb, group_in=group_to_update, user_in=user_to_remove
    )

    return schemas.Group.from_orm(updated_group)


@router.put("/{group_id}/add_plant", response_model=schemas.Group)
def group_add_plant(
    group_id: int,
    plant_id: int,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.Group:

    group_to_update = crud.group.get_by_id(timescaledb, group_id=group_id)
    plant_to_add = crud.plant.get_by_id(timescaledb, plant_id=plant_id)

    if not group_to_update or not plant_to_add:
        raise exceptions.not_found

    # check if user already is in group
    if plant_to_add in group_to_update.plants:
        return schemas.Group.from_orm(group_to_update)

    updated_group = crud.group.add_plant(
        timescaledb, group_in=group_to_update, plant_in=plant_to_add
    )

    return schemas.Group.from_orm(updated_group)


@router.put("/{group_id}/remove_plant", response_model=schemas.Group)
def group_remove_user(
    group_id: int,
    plant_id: int,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_superuser: models.User = Depends(get_current_active_superuser),
) -> schemas.Group:

    group_to_update = crud.group.get_by_id(timescaledb, group_id=group_id)
    plant_to_remove = crud.plant.get_by_id(timescaledb, plant_id=plant_id)

    if not group_to_update or not plant_to_remove:
        raise exceptions.not_found

    # check if user is in group
    if plant_to_remove not in group_to_update.plants:
        return schemas.Group.from_orm(group_to_update)

    updated_group = crud.group.remove_plant(
        timescaledb, group_in=group_to_update, plant_in=plant_to_remove
    )

    return schemas.Group.from_orm(updated_group)
