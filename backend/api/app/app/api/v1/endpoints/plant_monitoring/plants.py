from typing import Dict, Optional

from app import models, schemas
from app.api.deppendencies import get_current_active_user, get_timescaledb
from app.core import exceptions
from app.databases.timescaledb import crud
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

router = APIRouter(prefix="/plants")


@router.get("/", response_model=Dict[int, str])
async def get_all_plants(
    timescaledb: Session = Depends(get_timescaledb),
    current_active_user: models.User = Depends(get_current_active_user),
) -> Dict[int, str]:
    plants = crud.plant.get_all(timescaledb)
    # get only the names of the plants
    return {
        schemas.Plant.from_orm(plant).id: schemas.Plant.from_orm(plant).name
        for plant in plants
    }


@router.post("/create", response_model=schemas.Plant)
async def create_plant(
    plant_to_create: schemas.PlantCreate,
    current_active_superuser: models.User = Depends(get_current_active_user),
    timescaledb: Session = Depends(get_timescaledb),
) -> schemas.Plant:

    created_plant = crud.plant.create(timescaledb, schema_in=plant_to_create)

    if not created_plant:
        raise exceptions.database_writing

    return schemas.Plant.from_orm(created_plant)


@router.get("/{plant_id}", response_model=Optional[schemas.Plant])
async def get_plant_by_name(
    plant_id: int,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_user: models.User = Depends(get_current_active_user),
):
    plant = crud.plant.get_by_id(timescaledb, plant_id=plant_id)
    if not plant:
        raise exceptions.not_found

    # check if user has permission for plant
    if not crud.user.has_permission_for_plant(
        user_model=current_active_user, plant_model=plant
    ):
        raise exceptions.insufficient_permissions

    return schemas.Plant.from_orm(plant)


@router.get("/{plant_id}/timed", response_model=Optional[schemas.Plant])
async def get_plant_by_id_timed(
    plant_id: int,
    past_x_minutes: int = 60,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_user: models.User = Depends(get_current_active_user),
):
    plant = crud.plant.get_by_id(timescaledb, plant_id=plant_id)
    if not plant:
        raise exceptions.not_found

    # check if user has permission for plant
    if not crud.user.has_permission_for_plant(
        user_model=current_active_user, plant_model=plant
    ):
        raise exceptions.insufficient_permissions

    filtered_measurements = crud.measurement.get_since_by_plant(
        timescaledb, plant_id=plant.id, past_x_minutes=past_x_minutes
    )
    plant.measurements = filtered_measurements

    return schemas.Plant.from_orm(plant)


@router.put("/{plant_id}", response_model=schemas.PlantUpdated)
async def update_plant_by_id(
    plant_id: int,
    plant_in: schemas.PlantUpdate,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_user: models.User = Depends(get_current_active_user),
) -> schemas.PlantUpdated:
    plant_to_update = crud.plant.get_by_id(timescaledb, plant_id=plant_id)
    if not plant_to_update:
        raise exceptions.not_found

    # check if user has permission for plant
    if not crud.user.has_permission_for_plant(
        user_model=current_active_user, plant_model=plant_to_update
    ):
        raise exceptions.insufficient_permissions

    updated_plant = crud.plant.update(
        timescaledb, model_in=plant_to_update, schema_in=plant_in
    )

    return schemas.PlantUpdated.from_orm(updated_plant)


@router.post("/{plant_id}/add_measurement", response_model=schemas.Measurement)
async def add_measurement_to_plant_by_id(
    plant_id: int,
    measurement_in: schemas.MeasurementCreate,
    timescaledb: Session = Depends(get_timescaledb),
    current_active_user: models.User = Depends(get_current_active_user),
) -> schemas.Measurement:
    plant_to_update = crud.plant.get_by_id(timescaledb, plant_id=plant_id)
    if not plant_to_update:
        raise exceptions.not_found

    # check if user has permission for plant
    if not crud.user.has_permission_for_plant(
        user_model=current_active_user, plant_model=plant_to_update
    ):
        raise exceptions.insufficient_permissions

    created_measurement = crud.measurement.create_with_plant(
        timescaledb, schema_in=measurement_in, plant_id=plant_id
    )

    if not created_measurement:
        raise exceptions.database_writing

    return schemas.Measurement.from_orm(created_measurement)
