from app.api.v1.endpoints.account_management import groups, users
from app.api.v1.endpoints.plant_monitoring import plants
from fastapi import APIRouter

v1_router = APIRouter()

# account handling endpoints
v1_router.include_router(
    users.router, prefix="/account_management", tags=["account management"]
)
v1_router.include_router(
    groups.router, prefix="/account_management", tags=["account management"]
)

# plant monitoring endpoints
v1_router.include_router(
    plants.router, prefix="/plant_monitoring", tags=["plant monitoring"]
)
