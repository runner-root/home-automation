import secrets
from typing import Any, Dict, Optional

from pydantic import BaseSettings, EmailStr, PostgresDsn, validator


class Settings(BaseSettings):
    API_ACCESS_TOKEN_EXPIRE_MINUTES: int
    API_V1_ROOT_PATH: str
    API_SECRET_KEY: str = secrets.token_urlsafe(32)

    STACK_NAME: str

    POSTGRES_DB: str
    POSTGRES_PASSWORD: str
    POSTGRES_USER: str
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=f"{values.get('STACK_NAME')}_timescaledb",
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    SUPER_USER_EMAIL: EmailStr
    SUPER_USER_PASSWORD: str
    SUPER_USER_USERNAME: str

    class Config:
        case_sensitive = True
        env_file = "/.env"
        env_file_encoding = "utf-8"


settings: Settings = Settings()
