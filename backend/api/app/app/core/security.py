from datetime import datetime, timedelta
from typing import Any, Union

from app.core.settings import settings
from jose import jwt
from passlib.context import CryptContext

pwd_context: CryptContext = CryptContext(schemes=["bcrypt"], deprecated="auto")

ALGORITHM: str = "HS256"


def create_access_token(
    subject: Union[str, Any], expires_delta: timedelta = None
) -> str:
    if expires_delta:
        expire: datetime = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(
            minutes=settings.API_ACCESS_TOKEN_EXPIRE_MINUTES
        )
    to_encode: dict = {"exp": expire, "sub": str(subject)}
    encoded_jwt: str = jwt.encode(
        to_encode, settings.API_SECRET_KEY, algorithm=ALGORITHM
    )
    return encoded_jwt


def get_password_hash(pw_plain) -> Any:
    return pwd_context.hash(pw_plain)


def verify_password(plain_password, hashed_password) -> bool:
    return pwd_context.verify(plain_password, hashed_password)
