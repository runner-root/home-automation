from fastapi import HTTPException, status

invalid_credentials = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)

inactive_user = HTTPException(status_code=400, detail="Inactive user")

session_expired = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="session expired, please log in again",
    headers={"WWW-Authenticate": "Bearer"},
)

insufficient_permissions = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="insufficient permissions",
    headers={"WWW-Authenticate": "Bearer"},
)

database_writing = HTTPException(
    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    detail="unable to write data",
)

username_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="username already exists",
)

email_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="email already exists",
)

group_name_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="group name already exists",
)

not_found = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="whatever you are looking for, it is certainly not here",
)
