from typing import List, Optional

from pydantic import BaseModel

from .measurement import Measurement, MeasurementInDB


# Shared properties
class PlantBase(BaseModel):
    name: str
    location: str
    species: str


# Properties to receive on plant updating
class PlantCreate(PlantBase):
    pass


# Properties to receive on plant updating
class PlantUpdate(BaseModel):
    name: Optional[str] = None
    location: Optional[str] = None
    species: Optional[str] = None


# Properties shared by models stored in DB
class PlantInDBBase(PlantBase):
    id: int

    class Config:
        orm_mode = True


# Properties to return to client
class PlantUpdated(PlantInDBBase):
    pass


# Properties to return to client
class Plant(PlantInDBBase):
    groups: List = []
    measurements: List[Measurement] = []


# Properties properties stored in DB
class PlantInDB(PlantInDBBase):
    groups: List = []
    measurements: List[MeasurementInDB] = []
