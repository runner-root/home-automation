from typing import List, Optional

from pydantic import BaseModel


# Shared properties
class GroupBase(BaseModel):
    name: str


# Properties to receive via API on creation
class GroupCreate(GroupBase):
    pass


# Properties to receive via API on update
class GroupUpdate(GroupBase):
    pass


class GroupInDBBase(GroupBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# Additional properties to return via API
class Group(GroupInDBBase):

    plants: List = []
    users: List = []


# Additional properties stored in DB
class GroupInDB(GroupInDBBase):

    plants: List = []
    users: List = []
