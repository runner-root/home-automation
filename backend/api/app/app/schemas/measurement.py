from datetime import datetime

from pydantic import BaseModel


# Shared properties
class MeasurementBase(BaseModel):
    soil_humidity: int
    air_humidity: int
    air_pressure: int
    air_temperature: int
    brightness: int
    light_spectrum_red: int
    light_spectrum_orange: int
    light_spectrum_yellow: int
    light_spectrum_green: int
    light_spectrum_blue: int
    light_spectrum_violet: int
    power: int


# Properties to receive on measurement creation
class MeasurementCreate(MeasurementBase):
    pass


# Properties shared by models stored in DB
class MeasurementInDBBase(MeasurementBase):
    id: int
    created_utc: datetime

    class Config:
        orm_mode = True


# Properties to return to client
class Measurement(MeasurementInDBBase):
    pass


# Properties properties stored in DB
class MeasurementInDB(MeasurementInDBBase):
    pass
