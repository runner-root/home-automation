import logging

from app.databases.timescaledb.session import SessionLocal
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 5  # 5 minutes
wait_seconds = 1


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def check_timescaledb_status() -> None:
    try:
        timescaledb = SessionLocal()
        # Try to create session to check if DB is awake
        timescaledb.execute("SELECT 1")
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Waiting for timescaledb to start")
    check_timescaledb_status()
    logger.info("timescaledb started")


if __name__ == "__main__":
    main()
