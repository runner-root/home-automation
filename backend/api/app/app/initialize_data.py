import logging

from app.databases.timescaledb.init_db import init_db
from app.databases.timescaledb.session import SessionLocal

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def initialize_data_timescaledb() -> None:
    timescaledb: dict = SessionLocal()
    init_db(timescaledb)


def main() -> None:
    logger.info("Creating initial data for timescaledb")
    initialize_data_timescaledb()
    logger.info("Initial data for timescaledb created")


if __name__ == "__main__":
    main()
