from app.api.v1.api import v1_router
from app.core.settings import settings
from fastapi import FastAPI

app: FastAPI = FastAPI(
    docs_url=f"{settings.API_V1_ROOT_PATH}/docs",
    openapi_url=f"{settings.API_V1_ROOT_PATH}/openapi.json",
    redoc_url=None,
)

app.include_router(v1_router, prefix=settings.API_V1_ROOT_PATH)
