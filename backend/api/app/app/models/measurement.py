from app.databases.timescaledb.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, Integer
from sqlalchemy.orm import relationship


class Measurement(Base):
    id = Column(Integer, primary_key=True, index=True)
    created_utc = Column(DateTime, nullable=False)

    soil_humidity = Column(Integer, nullable=False)
    air_humidity = Column(Integer, nullable=False)
    air_pressure = Column(Integer, nullable=False)
    air_temperature = Column(Integer, nullable=False)
    brightness = Column(Integer, nullable=False)
    light_spectrum_red = Column(Integer, nullable=False)
    light_spectrum_orange = Column(Integer, nullable=False)
    light_spectrum_yellow = Column(Integer, nullable=False)
    light_spectrum_green = Column(Integer, nullable=False)
    light_spectrum_blue = Column(Integer, nullable=False)
    light_spectrum_violet = Column(Integer, nullable=False)
    power = Column(Integer, nullable=False)

    plant_id = Column(Integer, ForeignKey("plant.id"))
    plant = relationship("Plant", back_populates="measurements")
