from app.databases.timescaledb.base_class import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


class Group(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True, nullable=False)

    users = relationship("User", secondary="linkusergroup", back_populates="groups")
    plants = relationship("Plant", secondary="linkplantgroup", back_populates="groups")
