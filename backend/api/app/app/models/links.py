from app.databases.timescaledb.base_class import Base
from sqlalchemy import Column, ForeignKey, Integer


class LinkUserGroup(Base):
    group_id = Column(Integer, ForeignKey("group.id"), primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), primary_key=True)


class LinkPlantGroup(Base):
    group_id = Column(Integer, ForeignKey("group.id"), primary_key=True)
    plant_id = Column(Integer, ForeignKey("plant.id"), primary_key=True)
