from app.databases.timescaledb.base_class import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


class Plant(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)
    location = Column(String, nullable=False)
    species = Column(String, nullable=False)

    measurements = relationship("Measurement", back_populates="plant")
    groups = relationship("Group", secondary="linkplantgroup", back_populates="plants")
