<!--
api
api for plant monitoring

SPDX-FileCopyrightText: 2021 Maximilian Dolling

SPDX-License-Identifier: MIT

This program is free software: you can redistribute it and/or modify
it under the terms of the MIT license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You can find a copy of the license in the LICENSES folder.
If not, see <https://spdx.org/licenses/MIT.html>.
-->

# backend

## License

Copyright © 2021 Maximilian Dolling

This work is licensed under the following license(s):
* Everything else is licensed under [MIT](LICENSES/MIT.txt)

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).