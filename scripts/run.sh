#! /usr/bin/env sh

# Exit in case of error
set -e

# load variables
source "$PWD/.env"

# validate all variables are set
PROJECT_SLUG=${PROJECT_SLUG?Variable not set}
STAGE=${STAGE?Variable not set}
STACK_NAME=${STACK_NAME?Variable not set}
CONTAINER_REGISTRY=${CONTAINER_REGISTRY?Variable not set}
DOT_ENV_PATH=${DOT_ENV_PATH?Variable not set}

TRAEFIK_PUBLIC_NETWORK=${TRAEFIK_PUBLIC_NETWORK?Variable not set}
TRAEFIK_TAG=${TRAEFIK_TAG?Variable not set}
TRAEFIK_PUBLIC_PRODUCTION_DASHBOARD_PORT=${TRAEFIK_PUBLIC_PRODUCTION_DASHBOARD_PORT?Variable not set}
TRAEFIK_PUBLIC_PRODUCTION_HTTP_PORT=${TRAEFIK_PUBLIC_PRODUCTION_HTTP_PORT?Variable not set}
TRAEFIK_PUBLIC_STAGING_DASHBOARD_PORT=${TRAEFIK_PUBLIC_STAGING_DASHBOARD_PORT?Variable not set}
TRAEFIK_PUBLIC_STAGING_HTTP_PORT=${TRAEFIK_PUBLIC_STAGING_HTTP_PORT?Variable not set}
TRAEFIK_PUBLIC_TAG=${TRAEFIK_PUBLIC_TAG?Variable not set}

POSTGRES_DB=${POSTGRES_DB?Variable not set}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD?Variable not set}
POSTGRES_USER=${POSTGRES_USER?Variable not set}
TIMESCALEDB_TELEMETRY=${TIMESCALEDB_TELEMETRY?Variable not set}

API_ACCESS_TOKEN_EXPIRE_MINUTES=${API_ACCESS_TOKEN_EXPIRE_MINUTES?Variable not set}
API_SECRET_KEY=${API_SECRET_KEY?Variable not set}
API_V1_ROOT_PATH=${API_V1_ROOT_PATH?Variable not set}

SUPER_USER_EMAIL=${SUPER_USER_EMAIL?Variable not set}
SUPER_USER_PASSWORD=${SUPER_USER_PASSWORD?Variable not set}
SUPER_USER_USERNAME=${SUPER_USER_USERNAME?Variable not set}

# decide on the desired mode
if [ "$1" = '--help' ]; then
  echo '--build-dep'
  echo '--build'
  echo '--push'
  echo '--pull'
  echo '--deploy-local'
  echo '--deploy'
  echo '--stop'
  exit 0
fi

# login should be done before
docker login registry.gitlab.com

# set some advanced variables
if [ "$STAGE" = production ]; then
  export TRAEFIK_PUBLIC_DASHBOARD_PORT=$TRAEFIK_PUBLIC_PRODUCTION_DASHBOARD_PORT
  export TRAEFIK_PUBLIC_HTTP_PORT=$TRAEFIK_PUBLIC_PRODUCTION_HTTP_PORT
else
  export TRAEFIK_PUBLIC_DASHBOARD_PORT=$TRAEFIK_PUBLIC_STAGING_DASHBOARD_PORT
  export TRAEFIK_PUBLIC_HTTP_PORT=$TRAEFIK_PUBLIC_STAGING_HTTP_PORT
fi

if [ "$1" = '--build-dep' ]; then
  docker-compose -f docker-compose.dep.yml build
  docker-compose -f docker-compose.dep.yml push
  exit 0
fi

# generate stack yaml and replace some values
docker-compose -f docker-compose.app.yml config > stack.app.yml
sed -i '/name: /d' ./stack.app.yml
sed -i "s/REPLACE_TRAEFIK_PUBLIC_NETWORK/${TRAEFIK_PUBLIC_NETWORK}/g" ./stack.app.yml
sed -i "s/REPLACE_TIMESCALEDB_VOLUME/${STACK_NAME}-timescaledb-storage/g" ./stack.app.yml

if [ "$1" = '--build' ]; then
  docker-compose -f stack.app.yml build
  exit 0
fi

if [ "$1" = '--push' ]; then
  docker-compose -f stack.app.yml push
  exit 0
fi

if [ "$1" = '--pull' ]; then
  docker-compose -f stack.app.yml pull
  exit 0
fi

if [ "$1" = '--deploy-local' ]; then
  # tear down old stack
  docker stack rm "$STACK_NAME"
  # build app
  docker-compose -f stack.app.yml build
  # push app
  docker-compose -f stack.app.yml push
  # deploy local
  docker stack deploy -c stack.app.yml --with-registry-auth "$STACK_NAME"
  exit 0
fi

if [ "$1" = '--deploy' ]; then
  TARGET_PATH=${2?Variable not set}
  cp ./stack.app.yml "${TARGET_PATH}"/stack.app.yml
  docker stack deploy -c "${TARGET_PATH}"/stack.app.yml --with-registry-auth "$STACK_NAME"
  exit 0
fi

if [ "$1" = '--stop' ]; then
  docker stack rm "$STACK_NAME"
  exit 0
fi
